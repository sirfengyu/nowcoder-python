# nowcoder-python

# 输入模板


## 1. 单行输入
```｛python｝
a = input().split()
a1 = []
for k in a:
    # a1.append(k)#str
    a1.append(int(k))#int，得到list，推荐用下面那个一句话的
```
或

```｛python｝
line = list(map(int,input().split(' '))) #直接得到list
```
 或

```｛python｝
n = int(input())#输入只是一个数字
```


## 2. 多行输入


### 2.1. 行数已知，首行只有一个数字n
```｛python｝
'多行输入'
res = []
n = int(input())#行数
for _ in range(n):
    s = input()
    if s!='':
        temp = [j for j in s.split()] #str输入
        # temp = [int(j) for j in s.split()] #int输入
        res.append(temp[0])
    else:
        break
print(res)
```

### 2.2. 每行输出一个默（认）
```｛python｝
ef F(data):
    return data
 
'多行输入'
res = []
n = int(input())#行数
for _ in range(n):
    s = input()
    if s!='':
        temp = [j for j in s.split()] #str输入
        # temp = [int(j) for j in s.split()] #int输入
        res.append(temp[0])
    else:
        break
 
for i in res:
    print(F(i)) #每行只输出一个
    # print(F(i), end=' ') #全在同一行内输出，用空格隔开
```


## 3. 全在同一行内输出，用空格隔开

```｛python｝
def F(data):
    return data
 
'多行输入'
res = []
n = int(input())#行数
for _ in range(n):
    s = input()
    if s!='':
        temp = [j for j in s.split()] #str输入
        # temp = [int(j) for j in s.split()] #int输入
        res.append(temp[0])
    else:
        break
 
for i in res:
    # print(F(i)) #每行只输出一个
    print(F(i), end=' ') #全在同一行内输出，用空格隔开
```


## 4. 行数未知
```｛python｝

'多行输入，行数未知'
res = []
while True:
    try:
        s = input()
        # res.append(list(map(int, s.split(' '))))
        res.append(list(map(str, s.split(' '))))
    except:
        break
```

## 5. 首行有2个数字n，m 

往下n行数据对应n，再m行数据对应m 
```
info = list(map(int,input().split(' ')))
a = []
b = []
for i in range(info[0]):
    a.append(input().split(' '))
for j in range(info[1]):
    b.append(input().split(' '))
print(a)
print(b)
```


