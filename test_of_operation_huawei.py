"""
简单的 LISP 加减乘除语句解析并计算结果，四种运算符号为 add、sub、mul、div，分别为加减乘除。其中数字部分皆为整数。除法取整，除数为零输出 error。
例子：
(add 3 5 7) 结果为 15

(sub 1 9) 结果为 -8

(mul 0 9) 结果为 0

(div 8 3) 结果为 2

(div 8 0) 结果为 error

(add (sub (div 8 2) (mul 1 9)) 20) 结果为 15
思路：
使用 stack。
"""

class Stack(object):
    def __init__(self):
        self.stack = []

    def top(self):
        return self.stack[-1]

    def pop(self):
        return self.stack.pop()

    def push(self, value):
        self.stack.append(value)

    def is_empty(self):
        return len(self.stack) == 0

def calculate():
    s = input()
    stack = Stack()
    elements = []
    for i in s.split():
        if '(' in i:
            while '(' in i:
                elements.append('(')
                i = i[1:]
            elements.append(i)
        elif ')' in i:
            tmplist = []
            while ')' in i:
                tmplist.append(')')
                i = i[:-1]
            elements.append(i)
            elements += tmplist
        else:
            elements.append(i)
    print(elements)
    for i in elements:
        if i != ')':
            stack.push(i)
        else:
            tmp_list = []
            while stack.top() != '(':
                tmp_list.append(stack.pop())
            tmp_list.reverse()
            tmp_resault = int(tmp_list[1])
            if tmp_list[0] == 'add':
                print('add')
                for j in tmp_list[2:]:
                    tmp_resault += int(j)
            elif tmp_list[0] == 'sub':
                print('sub')
                for j in tmp_list[2:]:
                    tmp_resault -= int(j)
            elif tmp_list[0] == 'mul':
                print('mul')
                for j in tmp_list[2:]:
                    tmp_resault *= int(j)
            elif tmp_list[0] == 'div':
                print('div')
                for j in tmp_list[2:]:
                    if j == '0':
                        print("error")
                        return
                    else:
                        tmp_resault //= int(j)
            print(tmp_resault)
            stack.pop()
            stack.push(tmp_resault)
    print(stack.pop())

if __name__ == '__main__':
    calculate()



"""
class Stack(object):
    def __init__(self):
        self.stack = []

    def top(self):
        return self.stack[-1]

    def pop(self):
        return self.stack.pop()

    def push(self, value):
        self.stack.append(value)

    def is_empty(self):
        return len(self.stack) == 0

def parse(expression):
    mix_tokens = expression.split()
    tokens = []
    for mix_token in mix_tokens:
        mixer = ''
        for char in mix_token:
            if char == '(' or char == ')':
                if mixer != '':
                    tokens.append(mixer)
                    mixer = ''
                tokens.append(char)
            else:
                mixer += char
        if mixer != '':
            tokens.append(mixer)
    return tokens

def calculate(tokens):
    stack = Stack()
    for token in tokens:
        if token == ')':
            # 遇到右括号
            ans = None
            opt = None
            nums = []
            while not stack.is_empty():
                top = stack.pop()
                if top == 'add' or top == 'sub' or top == 'mul' or top == 'div':
                    # 计算符号
                    opt = top
                elif top == '(':
                    # 开始计算
                    nums.reverse()
                    for num in nums:
                        if ans is None:
                            ans = num
                        else:
                            if opt == 'add':
                                ans += num
                            elif opt == 'sub':
                                ans -= num
                            elif opt == 'mul':
                                ans *= num
                            elif opt == 'div':
                                if num == 0:
                                    return 'error'
                                ans //= num
                    break
                else:
                    # 其他数字
                    nums.append(int(top))
            # 计算结果重新入栈
            stack.push(ans)
        else:
            # 非右括号
            stack.push(token)
    return stack.top()

try:
    while True:
        expression = input()
        print(calculate(parse(expression)))
except:
    pass
"""



"""
找到一串字符串中字母出现次数最多次的字母。输入一串字符串，后面可接选项是否考虑大小写，预设为考虑。如果字母出现次数相同，输出字典序中最小的字母。
例子：
abcdeABCDEA 结果为 A 2

abcdeABCDEa 结果为 a 2

abcdeABCDE 结果为 a 1

abcdeABCDEA true 结果为 A 2

abcdeABCDEa true 结果为 a 2

abcdeABCDE true 结果为 a 1

abcdeABCDEA false 结果为 a 3

abcdeABCDEa false 结果为 a 3

abcdeABCDE false 结果为 a 2

思路：
使用 dict、sorted。
"""

"""
try:
    while True:
        string = input()
        opt = 'true'
        if ' ' in string:
            string, opt = string.split()
        if opt == 'false':
            string = string.lower()
        times = {}
        for char in string:
            if char in times:
                times[char] += 1
            else:
                times[char] = 1
        maxK = -1
        maxV = -1
        for k in sorted(times.keys(), reverse=True):
            if maxV <= times[k]:
                maxK = k
                maxV = times[k]
        print(maxK, maxV)
except:
    pass
"""