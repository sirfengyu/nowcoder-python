# 字符串处理
# HJ1	字符串最后一个单词的长度	字符串	较难	25.88%
"""
hello nowcoder
"""
def hj1():
    s = input()
    print(len(s.split()[-1]))

# if __name__ == '__main__':
#     hj1()

# HJ2	计算字符个数	字符串哈希	较难	24.08%
"""
ABCabc
A
"""
def hj2():
    # s = input()
    # c = input()
    # m = {}
    # for i in [j.lower() for j in s]:
    #     if i in m:
    #         m[i] = m[i] + 1
    #     else:
    #         m[i] = 1
    # if c.lower() in m:
    #     print(m[c.lower()])
    # else:
    #     print(0)
    s = input()
    c = input()
    print(s.lower().count(c.lower()))


# if __name__ == '__main__':
#     hj2()

# HJ4	字符串分隔	字符串	较难	23.31%
# HJ5	进制转换	字符串	中等	25.97%

"""
0xA
0xAA
"""

def hj5():
    while True:
        try:
            s = input()
            if s == '':
                break
            print(int(s,16))
        except EOFError:
            break

# if __name__ == '__main__':
#     hj5()

# HJ10	字符个数统计	字符串哈希	中等	37.20%

"""
输入
180
输出
2 2 3 3 5
"""


def hj10():
    l = []
    m = int(input())

    for i in range(2, m):
        while m % i == 0:
            l.append(i)
            m = int(m/i)
    print(" ".join(map(str, l)) + " " if l else str(m) + " ")


# if __name__ == '__main__':
#     hj10()


# HJ11	数字颠倒	字符串	简单	49.06%
"""
输入
1516000
输出
0006151
"""

def hj11():
    s = input()
    s = str(s)
    print(s[::-1])


# if __name__ == '__main__':
#     hj11()

# HJ12	字符串反转	字符串	简单	49.14%
"""
输入
abcd
输出
dcba
"""

def hj12():
    s = input()
    print(s[::-1])
# if __name__ == '__main__':
#     hj12()

# HJ14	字符串排序	字符串	中等	33.98%
"""
输入
9
cap
to
cat
card
two
too
up
boat
boot
输出
boat
boot
cap
card
cat
to
too
two
up
"""
def hj14():
    n = int(input())
    l = []
    for i in range(n):
        s = input()
        l.append(s)
    l.sort()
    for i in l:
        print(i)
# if __name__ == '__main__':
#     hj14()

# HJ17	坐标移动	字符串	较难	17.47%
"""
输入
A10;S20;W10;D30;X;A1A;B10A11;;A10;
输出
10,-10
"""
import re
def hj17():
    pattern = re.compile(r'([A|D|W|S])([0-9]+)$')
    s = input()
    z = [0, 0]
    for i in s.split(";"):
        if pattern.match(i):
            m = re.findall(pattern, i)[0]
            if m[0] == 'A':
                z[0] = z[0] - int(m[1])
            elif m[0] == 'D':
                z[0] = z[0] + int(m[1])
            elif m[0] == 'W':
                z[1] = z[1] + int(m[1])
            elif m[0] == 'S':
                z[1] = z[1] - int(m[1])

    print((",").join(list(map(str,z))))

if __name__ == '__main__':
    hj17()


# HJ18	识别有效的IP地址和掩码并进行分类统计	字符串查找	困难	18.38%
# HJ19	简单错误记录	字符串	困难	16.13%
# HJ20	密码验证合格程序	数组字符串	较难	23.30%
# HJ21	简单密码破解	字符串	中等	33.72%
# HJ23	删除字符串中出现次数最少的字符	字符串	较难	25.02%
# HJ26	字符串排序	排序字符串	中等	28.96%
# HJ29	字符串加解密	字符串	较难	22.45%
# HJ30	字符串合并处理	字符串排序	较难	22.49%
# HJ31	【中级】单词倒排	字符串排序	困难	16.05%
# HJ32	【中级】字符串运用-密码截取	字符串	较难	23.43%
# HJ33	整数与IP地址间的转换	字符串	较难	24.81%
# HJ34	图片整理	字符串	中等	36.94%
# HJ36	字符串加密	字符串	中等	32.91%
# HJ39	判断两个IP是否属于同一子网	字符串模拟	较难	12.53%
# HJ40	输入一行字符，分别统计出包含英文字母、空格、数字和其它字符的个数	字符串	中等	34.22%
# HJ41	称砝码	字符串	较难	33.13%
# HJ42	学英语	字符串	较难	26.00%
# HJ45	名字的漂亮度	字符串	中等	28.66%
# HJ46	按字节截取字符串	字符串	中等	36.26%
# HJ49	多线程	字符串链表栈队列查找	中等	33.77%
# HJ50	四则运算	字符串栈数学	简单	46.72%
# HJ52	计算字符串的距离	字符串	中等	31.97%
# HJ53	iNOC产品部-杨辉三角的变形	字符串	简单	39.92%
# HJ54	表达式求值	字符串数学栈	简单	47.73%
# HJ57	无线OSS－高精度整数加法	字符串	中等	35.86%
# HJ59	找出字符串中第一个只出现一次的字符	字符串	中等	24.39%
# HJ63	DNA序列	字符串	中等	32.97%
# HJ65	查找两个字符串a,b中的最长公共子串	字符串	中等	30.23%
# HJ70	矩阵乘法计算量估算	字符串	中等	34.95%
# HJ71	字符串通配符	字符串	中等	33.33%
# HJ73	计算日期到天数转换	字符串思维	简单	34.29%
# HJ74	参数解析	字符串	简单	43.85%
# HJ75	公共字串计算	字符串查找	简单	34.23%
# HJ81	字符串匹配	字符串	较难	24.40%
# HJ84	统计大写字母个数	字符串	简单	35.20%
# HJ85	字符串运用-密码截取	字符串穷举	简单	33.85%
# HJ88	扑克牌大小	字符串队列链表栈	中等	29.81%
# HJ89	24点运算	字符串模拟穷举	困难	13.91%
# HJ90	合法IP	字符串链表栈队列	中等	28.83%
# HJ91	201301 JAVA 题目2-3级	字符串	简单	34.32%
# HJ92	在字符串中找出连续最长的数字串	字符串	较难	22.83%
# HJ93	201301 JAVA题目0-1级	字符串递归	较难	27.81%
# HJ95	人民币转换	字符串	较难	24.26%
# HJ96	表示数字	字符串	中等	27.64%
# HJ98	自动售货系统	字符串	中等	22.71%
# HJ102	字符统计	字符串排序	中等	23.47%
# HJ106	字符逆序	字符串	简单	37.95%
